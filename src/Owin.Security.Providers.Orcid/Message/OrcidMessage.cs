﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Owin.Security.Providers.Orcid.Message
{
    public class OrcidIdentifier
    {

        [JsonProperty("value")]
        public object Value { get; set; }

        [JsonProperty("uri")]
        public string Uri { get; set; }

        [JsonProperty("path")]
        public string Path { get; set; }

        [JsonProperty("host")]
        public string Host { get; set; }
    }

    public class OrcidPreferences
    {

        [JsonProperty("locale")]
        public string Locale { get; set; }
    }

    public class SubmissionDate
    {

        [JsonProperty("value")]
        public long Value { get; set; }
    }

    public class LastModifiedDate
    {

        [JsonProperty("value")]
        public long Value { get; set; }
    }

    public class Claimed
    {

        [JsonProperty("value")]
        public bool Value { get; set; }
    }

    public class VerifiedEmail
    {

        [JsonProperty("value")]
        public bool Value { get; set; }
    }

    public class VerifiedPrimaryEmail
    {

        [JsonProperty("value")]
        public bool Value { get; set; }
    }

    public class OrcidHistory
    {

        [JsonProperty("creation-method")]
        public string CreationMethod { get; set; }

        [JsonProperty("completion-date")]
        public object CompletionDate { get; set; }

        [JsonProperty("submission-date")]
        public SubmissionDate SubmissionDate { get; set; }

        [JsonProperty("last-modified-date")]
        public LastModifiedDate LastModifiedDate { get; set; }

        [JsonProperty("claimed")]
        public Claimed Claimed { get; set; }

        [JsonProperty("source")]
        public object Source { get; set; }

        [JsonProperty("deactivation-date")]
        public object DeactivationDate { get; set; }

        [JsonProperty("verified-email")]
        public VerifiedEmail VerifiedEmail { get; set; }

        [JsonProperty("verified-primary-email")]
        public VerifiedPrimaryEmail VerifiedPrimaryEmail { get; set; }

        [JsonProperty("visibility")]
        public object Visibility { get; set; }
    }

    public class GivenNames
    {

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("visibility")]
        public object Visibility { get; set; }
    }

    public class FamilyName
    {

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("visibility")]
        public object Visibility { get; set; }
    }

    public class OtherNames
    {

        [JsonProperty("other-name")]
        public object[] OtherName { get; set; }

        [JsonProperty("visibility")]
        public string Visibility { get; set; }
    }

    public class PersonalDetails
    {

        [JsonProperty("given-names")]
        public GivenNames GivenNames { get; set; }

        [JsonProperty("family-name")]
        public FamilyName FamilyName { get; set; }

        [JsonProperty("credit-name")]
        public object CreditName { get; set; }

        [JsonProperty("other-names")]
        public OtherNames OtherNames { get; set; }
    }

    public class Biography
    {

        [JsonProperty("value")]
        public object Value { get; set; }

        [JsonProperty("visibility")]
        public string Visibility { get; set; }
    }

    public class ResearcherUrls
    {

        [JsonProperty("researcher-url")]
        public object[] ResearcherUrl { get; set; }

        [JsonProperty("visibility")]
        public string Visibility { get; set; }
    }

    public class Email
    {

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("primary")]
        public bool Primary { get; set; }

        [JsonProperty("current")]
        public bool Current { get; set; }

        [JsonProperty("verified")]
        public bool Verified { get; set; }

        [JsonProperty("visibility")]
        public string Visibility { get; set; }

        [JsonProperty("source")]
        public string Source { get; set; }

        [JsonProperty("source-client-id")]
        public object SourceClientId { get; set; }
    }

    public class ContactDetails
    {

        [JsonProperty("email")]
        public Email[] Email { get; set; }

        [JsonProperty("address")]
        public object Address { get; set; }
    }

    public class ExternalIdentifiers
    {

        [JsonProperty("external-identifier")]
        public object[] ExternalIdentifier { get; set; }

        [JsonProperty("visibility")]
        public string Visibility { get; set; }
    }

    public class OrcidBio
    {

        [JsonProperty("personal-details")]
        public PersonalDetails PersonalDetails { get; set; }

        [JsonProperty("biography")]
        public Biography Biography { get; set; }

        [JsonProperty("researcher-urls")]
        public ResearcherUrls ResearcherUrls { get; set; }

        [JsonProperty("contact-details")]
        public ContactDetails ContactDetails { get; set; }

        [JsonProperty("keywords")]
        public object Keywords { get; set; }

        [JsonProperty("external-identifiers")]
        public ExternalIdentifiers ExternalIdentifiers { get; set; }

        [JsonProperty("delegation")]
        public object Delegation { get; set; }

        [JsonProperty("scope")]
        public object Scope { get; set; }
    }

    public class OrcidProfile
    {

        [JsonProperty("orcid")]
        public object Orcid { get; set; }

        [JsonProperty("orcid-id")]
        public object OrcidId { get; set; }

        [JsonProperty("orcid-identifier")]
        public OrcidIdentifier OrcidIdentifier { get; set; }

        [JsonProperty("orcid-deprecated")]
        public object OrcidDeprecated { get; set; }

        [JsonProperty("orcid-preferences")]
        public OrcidPreferences OrcidPreferences { get; set; }

        [JsonProperty("orcid-history")]
        public OrcidHistory OrcidHistory { get; set; }

        [JsonProperty("orcid-bio")]
        public OrcidBio OrcidBio { get; set; }

        [JsonProperty("orcid-activities")]
        public object OrcidActivities { get; set; }

        [JsonProperty("orcid-internal")]
        public object OrcidInternal { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("group-type")]
        public object GroupType { get; set; }

        [JsonProperty("client-type")]
        public object ClientType { get; set; }
    }

    public class OrcidProfileMessage
    {

        [JsonProperty("message-version")]
        public string MessageVersion { get; set; }

        [JsonProperty("orcid-profile")]
        public OrcidProfile OrcidProfile { get; set; }

        [JsonProperty("error-desc")]
        public object ErrorDesc { get; set; }
    }

    public partial class OrcidEmployments
    {
        [JsonProperty("last-modified-date")]
        public EdDate LastModifiedDate { get; set; }

        [JsonProperty("employment-summary")]
        public List<EmploymentSummary> EmploymentSummary { get; set; }

        [JsonProperty("path")]
        public string Path { get; set; }
    }

    public partial class EmploymentSummary
    {
        [JsonProperty("created-date")]
        public EdDate CreatedDate { get; set; }

        [JsonProperty("last-modified-date")]
        public EdDate LastModifiedDate { get; set; }

        [JsonProperty("source")]
        public Source Source { get; set; }

        [JsonProperty("department-name")]
        public string DepartmentName { get; set; }

        [JsonProperty("role-title")]
        public string RoleTitle { get; set; }

        [JsonProperty("start-date")]
        public Date StartDate { get; set; }

        [JsonProperty("end-date")]
        public Date EndDate { get; set; }

        [JsonProperty("organization")]
        public Organization Organization { get; set; }

        [JsonProperty("visibility")]
        public string Visibility { get; set; }

        [JsonProperty("put-code")]
        public long PutCode { get; set; }

        [JsonProperty("path")]
        public string Path { get; set; }
    }

    public partial class EdDate
    {
        [JsonProperty("value")]
        public long Value { get; set; }
    }

    public partial class Date
    {
        [JsonProperty("year")]
        public Day Year { get; set; }

        [JsonProperty("month")]
        public Day Month { get; set; }

        [JsonProperty("day")]
        public Day Day { get; set; }
    }

    public partial class Day
    {
        [JsonProperty("value")]
        public string Value { get; set; }
    }

    public partial class Organization
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("address")]
        public Address Address { get; set; }

        [JsonProperty("disambiguated-organization")]
        public DisambiguatedOrganization DisambiguatedOrganization { get; set; }
    }

    public partial class Address
    {
        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("region")]
        public string Region { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }
    }

    public partial class DisambiguatedOrganization
    {
        [JsonProperty("disambiguated-organization-identifier")]
        public string DisambiguatedOrganizationIdentifier { get; set; }

        [JsonProperty("disambiguation-source")]
        public string DisambiguationSource { get; set; }
    }

    public partial class Source
    {
        [JsonProperty("source-orcid")]
        public SourceOrcid SourceOrcid { get; set; }

        [JsonProperty("source-client-id")]
        public object SourceClientId { get; set; }

        [JsonProperty("source-name")]
        public Day SourceName { get; set; }
    }

    public partial class SourceOrcid
    {
        [JsonProperty("uri")]
        public string Uri { get; set; }

        [JsonProperty("path")]
        public string Path { get; set; }

        [JsonProperty("host")]
        public string Host { get; set; }
    }


    /* Bens additions for ORCiD 2.1 */
    public class Orcid20ProfileMessage
    {

        [JsonProperty("last-modified-date")]
        public EdDate LastModifiedDate { get; set; }

        [JsonProperty("name")]
        public Orcid20Name Name { get; set; }

        [JsonProperty("other-names")]
        public Orcid20OtherNames OtherNames { get; set; }

        [JsonProperty("biography")]
        public Orcid20Biography Biography { get; set; }

        [JsonProperty("emails")]
        public Orcid20Emails Emails { get; set; }

    }

    public class Orcid20Name
    {

        [JsonProperty("created-date")]
        public EdDate CreatedDate { get; set; }

        [JsonProperty("last-modified-date")]
        public EdDate LastModifiedDate { get; set; }

        [JsonProperty("given-names")]
        public Orcid20GivenNames GivenNames { get; set; }

        [JsonProperty("family-name")]
        public Orcid20FamilyName FamilyName { get; set; }

        [JsonProperty("credit-name")]
        public object CreditName { get; set; }

        [JsonProperty("source")]
        public object Source { get; set; }

        [JsonProperty("visibility")]
        public String Visibility { get; set; }

        [JsonProperty("path")]
        public String Path { get; set; }

    }

    public class Orcid20GivenNames
    {
        [JsonProperty("value")]
        public string Value { get; set; }

    }

    public class Orcid20FamilyName
    {

        [JsonProperty("value")]
        public string Value { get; set; }
    }

    public class Orcid20OtherNames
    {

        [JsonProperty("other-name")]
        public object[] OtherName { get; set; }
    }
    public class Orcid20Biography
    {

        [JsonProperty("created-date")]
        public EdDate CreatedDate { get; set; }

        [JsonProperty("last-modified-date")]
        public EdDate LastModifiedDate { get; set; }

        [JsonProperty("content")]
        public String Content { get; set; }

        [JsonProperty("visibility")]
        public String Visibility { get; set; }

        [JsonProperty("path")]
        public String Path { get; set; }

    }

    public class Orcid20Emails
    {

        [JsonProperty("last-modified-date")]
        public EdDate LastModifiedDate { get; set; }

        [JsonProperty("email")]
        public List<Orcid20Email> Email { get; set; }


        [JsonProperty("path")]
        public String Path { get; set; }
    }

    public class Orcid20Email
    {
        [JsonProperty("created-date")]
        public EdDate CreatedDate { get; set; }

        [JsonProperty("last-modified-date")]
        public EdDate LastModifiedDate { get; set; }

        [JsonProperty("email")]
        public String Email { get; set; }

        [JsonProperty("path")]
        public String Path { get; set; }

        [JsonProperty("visibility")]
        public String Visibility { get; set; }

        [JsonProperty("verified")]
        public bool Verified { get; set; }

        [JsonProperty("primary")]
        public bool Primary { get; set; }
    }
}
