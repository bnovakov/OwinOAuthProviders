﻿using Microsoft.Owin;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Owin.Security.Providers.Orcid.Message
{
	public static class OrcidMessageExtensions
	{
		public static OrcidAuthenticatedContext ToAuthenticationContext(this string json, IOwinContext context, string orcid, string accessToken)
		{
			var profile = JsonConvert.DeserializeObject<Orcid20ProfileMessage>(json);

			var user = JObject.Parse(json);

			var authenticatedContext = new OrcidAuthenticatedContext(context, user, accessToken);

            var email = GetPrimaryEmail(profile);
            //var email = profile.OrcidProfile.OrcidBio?.ContactDetails?.Email?.LastOrDefault();
            if (email != null)
                authenticatedContext.Email = email;

			authenticatedContext.Id = orcid;
			authenticatedContext.UserName = orcid;

            //authenticatedContext.FirstName = profile.OrcidProfile.OrcidBio.PersonalDetails.GivenNames.Value;
            //authenticatedContext.LastName = profile.OrcidProfile.OrcidBio.PersonalDetails.FamilyName.Value;
            authenticatedContext.FirstName = profile.Name.GivenNames.Value;
            authenticatedContext.LastName = profile.Name.FamilyName.Value;

            return authenticatedContext;
		}

        public static string GetLatestOrganization(this string json)
        {
            string result = "";

            var employments = JsonConvert.DeserializeObject<OrcidEmployments>(json);

            if (employments != null)
            {
                if (employments.EmploymentSummary != null)
                {
                    if (employments.EmploymentSummary.Count > 0)
                    {
                        if (employments.EmploymentSummary[0].Organization != null)
                        {
                            result = employments.EmploymentSummary[0].Organization.Name;
                        }
                    }
                }
            }

            return result;
        }

        public static string GetPrimaryEmail(this Orcid20ProfileMessage profile)
        {
            string result = "";

            if(profile.Emails.Email.Count == 0)
            {
                return null;
            }
            foreach(var e in profile.Emails.Email)
            {
                if (e.Primary == true)
                {
                    result = e.Email;
                }
            }
            if(result == "")
            {
                result = profile.Emails.Email[0].Email;
            }
        
            return result;
        }
    }
}